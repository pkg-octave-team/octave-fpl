octave-fpl (1.3.5-7) unstable; urgency=medium

  * d/copyright:
    + Accentuate my family name
    + Update Copyright years for debian/* files
  * Set upstream metadata fields: Archive.
  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/watch: Adjust for new URL at gnu-octave.github.io
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector

 -- Rafael Laboissière <rafael@debian.org>  Mon, 05 Dec 2022 05:58:47 -0300

octave-fpl (1.3.5-6) unstable; urgency=medium

  * d/control:
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Bump debhelper compatibitlity level to 13
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Tue, 28 Jul 2020 05:50:23 -0300

octave-fpl (1.3.5-5) unstable; urgency=medium

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

  [ Rafael Laboissiere ]
  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:55:32 -0200

octave-fpl (1.3.5-4) unstable; urgency=medium

  * Use dh-octave for building the package
  * d/control:
     + Use Debian's GitLab URLs in Vcs-* headers
     + Change Maintainer to team+pkg-octave-team@tracker.debian.org

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 10 Feb 2018 07:29:41 -0200

octave-fpl (1.3.5-3) unstable; urgency=medium

  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:13:27 -0200

octave-fpl (1.3.5-2) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * d/control:
    + Use secure URIs in the Vcs-* fields
    + Bump Standards-Version to 4.0.1 (no changes needed)
    + Use cgit instead of gitweb in Vcs-Browser URL

  [ Sébastien Villemot ]
  * d/copyright: use secure URL for format.
  * d/watch: bump to format version 4.

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 18 Aug 2017 12:03:37 -0300

octave-fpl (1.3.5-1) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.3.5
  * d/p/autoload-yes.diff: Remove patch (deprecated upstream)
  * d/p/no-interactive-tests.diff: Remove obsolete patch

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 10 Sep 2015 20:32:22 +0200

octave-fpl (1.3.4-2) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Add Rafael Laboissiere and Mike Miller to Uploaders.

  [ Rafael Laboissiere ]
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Mention all upstream files in debian/copyright (fix Lintian warning)

 -- Rafael Laboissiere <rafael@laboissiere.net>  Sat, 27 Sep 2014 03:48:11 -0300

octave-fpl (1.3.4-1) unstable; urgency=low

  [ Sébastien Villemot ]
  * Imported Upstream version 1.3.4

  [ Thomas Weber ]
  * debian/control: Use canonical URLs in Vcs-* fields

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 03 Aug 2013 09:55:47 +0000

octave-fpl (1.3.3-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Use the octave-maintainers mailing list as upstream
    contact

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 20 May 2013 11:34:25 +0200

octave-fpl (1.3.3-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.3.3
  * debian/patches/autoload-yes.diff: Refresh for the new upstream version
  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Use Sébastien Villemot's @debian.org email address
  * Remove obsolete DM-Upload-Allowed flag
  * debian/copyright: Simplify the upstream stanzas

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 04 Nov 2012 11:47:40 +0100

octave-fpl (1.3.2-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.3.2
  * debian/control: Change Architecture to 'all'
  * debian/patches/autoload-yes.diff: Adjust for new upstream release
  * debian/copyright: Reflect upstream changes

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Fri, 14 Sep 2012 07:35:33 +0000

octave-fpl (1.3.1-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.3.1
  * debian/copyright: Reflect upstream changes
  * debian/patches/autoload-yes.diff: Adjust for new upstream version

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Sun, 19 Aug 2012 19:35:59 +0000

octave-fpl (1.3.0-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.3.0
  * debian/patches/autoload-yes.diff: Adjust for new upstream version
  * debian/patches/no-interactive-tests.diff: Adjust for new upstream version
  * Drop dependency on dx
  * Make the package Architecture: any
  * debian/watch: Use the SourceForge redirector
  * debian/copyright: Reflect upstream changes, fix Source URL

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Fri, 10 Aug 2012 09:46:13 +0000

octave-fpl (1.2.0-3) unstable; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.2.0
  * Bump to debhelper compat level 9
  * Build-depend on octave-pkg-dev >= 1.0.1, to build against Octave 3.6
  * Bump to Standards-Version 3.9.3, no changes needed
  * Add Sébastien Villemot to the list of Uploaders
  * debian/copyright: update to machine-readable format 1.0

 -- Thomas Weber <tweber@debian.org>  Fri, 23 Mar 2012 19:42:02 +0100

octave-fpl (1.2.0-2) unstable; urgency=low

  * Upload to unstable

 -- Thomas Weber <tweber@debian.org>  Sun, 10 Apr 2011 17:14:47 +0200

octave-fpl (1.2.0-1) experimental; urgency=low

  * New upstream release

 -- Thomas Weber <tweber@debian.org>  Wed, 01 Sep 2010 22:02:26 +0200

octave-fpl (1.0.0-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Remove Rafael Laboissiere from Uploaders (Closes: #571843)
    - Remove Ólafur Jens Sigurðsson <ojsbug@gmail.com> from Uploaders
  * Bump standards version to 3.8.4, no changes needed
  * Switch to dpkg-source 3.0 (quilt) format
  * Drop patch depends-octave.diff, applied upstream

 -- Thomas Weber <tweber@debian.org>  Thu, 13 May 2010 23:11:00 +0200

octave-fpl (0.1.6-2) unstable; urgency=low

  * Upload to unstable.

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Wed, 11 Nov 2009 22:42:57 +0100

octave-fpl (0.1.6-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * debian/control: Build-depend on octave-pkg-dev >= 0.7.0, such that the
    package is built against octave3.2

  [ Thomas Weber ]
  * New upstream release
  * Bump standards version to 3.8.2, no changes needed
  * debian/patches/no-echo-command.diff: removed, applied upstream

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Wed, 22 Jul 2009 14:24:39 +0200

octave-fpl (0.1.5-1) unstable; urgency=low

  * Initial release. (Closes: #468537)

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 24 May 2009 10:54:46 +0200
